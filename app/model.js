var pg = require('pg'),
    model = module.exports,
    connString = process.env.DATABASE_URL;

var pool = new pg.Pool({connectionString: connString});

model.getAccessToken = function (bearerToken) {
    return pool.connect().then(function (c) {
        return c.query('SELECT access_token, client_id, expires, user_id, username, firstname, lastname FROM oauth_access_token join user_account on user_account.id = user_id WHERE access_token = $1', [bearerToken]).then(function (result) {
            c.release();
            var token = result.rows[0];
            return {
                accessToken: token.access_token,
                client: {id: token.client_id},
                accessTokenExpiresAt: token.expires,
                user: {id: token.user_id, username: token.username, firstname: token.firstname, lastname: token.lastname}
            };
        });
    });
};

model.getClient = function (clientId, clientSecret) {
    return pool.connect().then(function (c) {
        return c.query('SELECT client_id, client_secret, redirect_uri FROM oauth_client WHERE client_id = $1', [clientId]).then(function (result) {
            c.release();
            var r = result.rows[0];
            return {
                id : r.client_id,
                redirectUris: [r.redirect_uri],
                grants: ['authorization_code']
            };
        });
    });
};

model.getClientName = function(clientId) {
    return pool.connect().then(function (c) {
        return c.query('SELECT client_id, client_name FROM oauth_client WHERE client_id = $1', [clientId]).then(function (result) {
            c.release();
            var r = result.rows[0];
            return r.client_name;
        });
    });
};

model.revokeAuthorizationCode = function(code) {
    return pool.connect().then(function (c) {
        return c.query('DELETE FROM oauth_auth_code WHERE auth_code = $1', [code.code]).then(function (result) {
            c.release();
            return true;
        });
    });
};

model.getAuthorizationCode = function (authCode) {
    return pool.connect().then(function (c) {
        return c.query('SELECT auth_code, client_id as "clientId", expires, user_id as "userId" FROM oauth_auth_code WHERE auth_code = $1', [authCode]).then(function (result) {
            c.release();
            r = result.rows[0]
            return {
                code: r.auth_code,
                expiresAt: r.expires,
                client: { id: r.clientId },
                user: { id: r.userId }
            };
        });
    });
};

model.saveAuthorizationCode= function (authCode, client, user) {
    return pool.connect().then(function(c) {
        return c.query('INSERT INTO oauth_auth_code(auth_code, client_id, user_id, expires) VALUES ($1, $2, $3, $4)', [authCode.authorizationCode, client.id, user.id, authCode.expiresAt]).then(function (result) {
            c.release();
            return {
                authorizationCode: authCode.authorizationCode,
                expiresAt: authCode.expiresAt,
                redirectUri: authCode.redirectUri,
                client: client,
                user: user
            };
        });
    });
};

model.saveToken = function (token, client, user) {
    return pool.connect().then(function (c) {
        return c.query('INSERT INTO oauth_access_token(access_token, client_id, user_id, expires) VALUES ($1, $2, $3, $4)', [token.accessToken, client.id, user.id, token.accessTokenExpiresAt]).then(function (result) {
            c.release();
            return {
                accessToken: token.accessToken,
                accessTokenExpiresAt: token.accessTokenExpiresAt,
                client: client,
                user: user
            };
        });
    });
};

model.getUser = function (username, password) {
    return pool.connect().then(function (c) {
        return c.query('select id, username, password, array_agg(mobile) as mobiles from user_account join user_mobile on user_account.id = user_mobile.user_id where username = $1 AND password = $2 group by id', [username, password]).then(function (result) {
            c.release();
            return result.rows[0];
        });
    });
};
