var axios = require('axios');

var Factern = function(appId, appKey, userId) {
    this.appId = appId;
    this.appKey = appKey;
    this.userId = userId;
    this.params = {
        user: userId, 
        operatingAs: userId
    };
    this.base = "https://api.factern.com/v2/";
    this.bearerToken = this.initBearerToken();
};

Factern.prototype.post = function(endpoint, data) {
    var fac = this;
    return this.bearerToken.then(function(bearerToken) {
        var headers = {
            "Accept": "application/json", 
            "Content-Type": "application/json", 
            "Authorization": "Bearer " + bearerToken.data.access_token
        };
        var options = {
            params: fac.params,
            headers: headers
        };
        return axios.post(fac.base + endpoint, data, options).then(function(results) {
            return results.data;
        }).catch(function(err) {
            console.log(err);
            throw(err);
        });
    });
};

Factern.prototype.initBearerToken = function() {
    var endpoint = "https://sso.factern.com/auth/token";
    var data = { grant_type: "client_credentials" };
    var auth = {
        username: this.appId,
        password: this.appKey
    };
    return axios.post(endpoint, {}, { params: data, auth: auth });
};

Factern.prototype.describe = function(nodeId) {
    var data = { nodeId: nodeId };
    return this.post("describe", data);
};

Factern.prototype.deletenode = function(nodeId) {
    var data = { nodeId: nodeId };
    return this.post("deletenode", data);
};

Factern.prototype.createEntity = function(parentId, { name = undefined } = {}) {
    var data = { parentId: parentId };
    if (name) data.name = name;
    return this.post("createentity", data);
};

Factern.prototype.createField = function(parentId, name, { description = "", searchable = false, uniqueByParent = true, branch = false } = {}) {
    var data = {
        parentId: parentId,
        name: name,
        description: description,
        searchable: searchable,
        uniqueByParent, uniqueByParent,
        branch: branch
    };
    return this.post("createfield", data);
};

Factern.prototype.createInformation = function(parentId, fieldId, info) {
    var data = { parentId: parentId, fieldId: fieldId, data: info };
    return this.post("createinformation", data);
};

Factern.prototype.createLabelList = function(parentId, name, members) {
    var data = { parentId: parentId, name: name, members: members };
    return this.post("createlabellist", data);
};

Factern.prototype.createTemplate = function(parentId, name, template) {
    var data = { parentId: parentId, name: name, memberIds: template };
    return this.post("createtemplate", data);
};

Factern.prototype.labelNode = function(targetNodeId, labelId) {
    var data = { targetNodeId: targetNodeId, labelId: labelId };
    return this.post("label", data);
};

Factern.prototype.listChildren = function(nodeType, nodeId) {
    var data = {
        nodeId: nodeId,
        listChildren: { factType: nodeType }
    };
    return this.post("describe", data).then(function(results) {
        return results.children.nodes;
    });
};

Factern.prototype.listLabels = function(nodeId, labelListId) {
    var data = {
        nodeId: nodeId,
        listChildren: { factType: "Label", "labelListId": labelListId }
    };
    return this.post("describe", data).then(function(results) {
        return results.children.nodes;
    });
};

Factern.prototype.readInlineTemplate = function(parentId, template) {
    var data = { nodeId: parentId, template: template };
    return this.post("read", data);
};

Factern.prototype.readTemplate = function(parentId, templateId) {
    var data = { nodeId: parentId, templateId: templateId };
    return this.post("read", data);
};

Factern.prototype.writeDocument = function(parentId, doc) {
    var data = { nodeId: parentId, "document": doc };
    return this.post("write", data);
};
Factern.prototype.writeInlineTemplate = function(parentId, template, values) {
    var data = { nodeId: parentId, template: template, values: values };
    return this.post("write", data);
};

Factern.prototype.writeTemplate = function(parentId, templateId, values) {
    var data = { nodeId: parentId, templateId: templateId, values: values };
    return this.post("write", data);
};

module.exports.Factern = Factern;
