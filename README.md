# Digital Passport Demo: Server #

The Digital Passport demo has been developed by AgFeTe to showcase how we can build on the [Factern](https://factern.com%20/%20Factern) ecosystem with ease to store structured, metadata-rich information.  This information can then be conveniently and securely shared.  Individuals and SMEs can both benefit from holding a Digital Passport to store sensitive information (such as their full name, address and historic addresses) which they frequently need to share with third parties.

### What is this repository for? ###

Our Digital Passport Server

* sets up the Digital Passport data structure on Factern
* writes templated data to Factern
* tags Factern nodes with additional metadata to record when data has been confirmed by trusted third parties
* reads templated data from Factern

### Getting started ###

Some environment variables are required to define your configuration:

    export DATABASE_URL='postgresql://user:pass@server:port/dbname' 
    export FACTERN_APP_ID='YOUR_FACTERN_APP_ID'
    export FACTERN_APP_KEY='YOUR_FACTERN_APP_KEY'                    
    export FACTERN_USER_ID='YOUR_FACTERN_USER_ID'
    export SESSION_SECRET='YOUR_SESSION_SECRET'

To start the server run `npm start`.  The server will run on port 8081.

### More information ###

* For more information on the [Factern](https://factern.com%20/%20Factern) project or this repository, please write to us at [discovery@factern.com](mailto:discovery@factern.com)