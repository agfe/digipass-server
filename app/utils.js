var utils = module.exports;

var metaLabel = "MetaLabel";
var userLabel = "UserLabel";
var definitionRootLabel = "definition_root";
var labelListRootLabel = "labellist_root";
var typesRootLabel = "types_root";
var templateRootLabel = "template_root";
var dataRootLabel = "data_root";
var definitionRootLookup = "[" + metaLabel + "=" + definitionRootLabel + "]";

var templates = {
    "BasicDetails": [{
        ":BasicDetails" : [
            {":FullName": [":Title", ":GivenNames", ":Surname"]},
            ":EmailAddress",
            ":MobilePhone",
            ":DateOfBirth"
        ]
    }],
    "HomeAddress": [{
        ":HomeAddress": [
            {":Address": [":HouseNoName", ":AddressLine1", ":AddressLine2", ":City", ":County", ":Postcode"]}
        ]
    }],
    "BankDetails": [{
        ":BankDetails": [
            ":SortCode", 
            ":AccountNo", 
            ":Bank"
        ]
    }],
    "Passport": [{
        ":Passport": [
            ":PassportNo"
        ]
    }],
    "CompanyInformation": [{
        ":CompanyInformation": [
            ":FullLegalName",
            ":TradingName",
            ":LegalStatus",
            ":CompanyRegistrationNumber",
            ":CountryOfIncorporation"
        ]
    }],
    "CompanyNatureOfBusiness": [{
        ":CompanyNatureOfBusiness": [
            ":PrimaryProductsAndServices",
            ":BusinessCommencementDate",
            ":NumberOfOffices",
            ":MainGeographicalMarkets",
            ":SourceOfFunds"
        ],
    }],
    "ManagementAccounts": [{
        ":ManagementAccounts": [
            ":ProfitAndLoss",
            ":BalanceSheet"
        ],
    }],
    "CompanyContactDetails": [{
        ":CompanyContactDetails": [{
            ":RegisteredAddress": [
                ":RegisteredAddressLine1",
                ":RegisteredAddressLine2",
                ":RegisteredAddressLocality",
                ":RegisteredAddressPostcode"
            ]
        },{
            ":TradingAddress": [
                ":TradingAddressLine1",
                ":TradingAddressLine2",
                ":TradingAddressLocality",
                ":TradingAddressPostcode"
            ]
        }]
    }]
};

var confirmationStatusName = "ConfirmStatus";

utils.deleteAll = function(fac, nodeId, deleteRoot) {
    var promises = [];
    ["Field", "LabelListMember", "Member", "Information"].forEach(function(nodeType) {
        promises.push(fac.listChildren(nodeType, nodeId).then(function(r) {
            var deletions = [];
            r.forEach(function(c) {
                deletions.push(fac.deletenode(c.nodeId).catch(function(err) { return err; }));
            });
            return Promise.all(deletions);
        }));
    });
    ["LabelList", "Template", "Entity"].forEach(function(nodeType) {
        promises.push(fac.listChildren(nodeType, nodeId).then(function(r) {
            var deletions = [];
            r.forEach(function(c) {
                deletions.push(utils.deleteAll(fac, c.nodeId, true));
            });
            return Promise.all(deletions);
        }));
    });
    if (deleteRoot) {
        return Promise.all(promises).then(function(rr) {
            return fac.deletenode(nodeId);
        });
    } else {
        return Promise.all(promises);
    }
};

utils.createLabelledEntity = function(fac, parentId, labelName, label, { createMetaLabelField = false } = {} ) {
    return fac.createEntity(parentId).then(function(r) {
        var nodeId = r.nodeId;
        var promises = [];
        if (createMetaLabelField)
            promises.push(fac.createField(nodeId, labelName, { searchable: true }));
        return Promise.all(promises).then(function(pr) {
            return fac.createInformation(nodeId, ":" + labelName, label).then(function(cir) {
                return r;
            });
        });
    });
};

utils.createFieldTypes = function(fac, template, parentId) {
    var promises = [];
    template.forEach(function(item, idx) {
        if (typeof item === 'string' || item instanceof String)
            promises.push(fac.createField(parentId, item.replace(/^:/, ''), { uniqueByParent: true }));
        else {
            var name = Object.keys(item)[0];
            promises.push(fac.createField(parentId, name.replace(/^:/, ''), { branch: true }).then(function(r) {
                return utils.createFieldTypes(fac, item[name], parentId);
            }));
        }
    });
    return Promise.all(promises);
};

utils.coalesceTemplateRead = function(fac, template, response) {
    var res = [];
    template.forEach(function(item, idx) {
        if (response[idx].status == 200) {
            var responsePart = response[idx].readItem;
            var resultPart = {};
            if (typeof item === 'string' || item instanceof String) {
                resultPart[item] = {data: responsePart.data, nodeId: responsePart.nodeId};
            } else {
                var name = Object.keys(item)[0];
                resultPart[name] = utils.coalesceTemplateRead(fac, item[name], responsePart.children);
            }
            res.push(resultPart);
        }
    });
    return res;
};

utils.coalesceTemplateReadName = function(fac, templateName, response) {
    var template = templates[templateName];
    return utils.coalesceTemplateRead(fac, template, response.items);
};

utils.addConfirmationStatus = function(fac, data) {
    var promises = [];
    data.forEach(function(item) {
        var name = Object.keys(item)[0];
        if (Array.isArray(item[name]))
            promises.push(utils.addConfirmationStatus(fac, item[name]).then(function(r) {
                var resultItem = {};
                resultItem[name] = r;
                return resultItem;
            }));
        else {
            promises.push(fac.listLabels(item[name].nodeId, "frn:labellist::" + confirmationStatusName).then(function(ll) {
                if (ll.length > 0) {
                    var resultItem = item;
                    prom = fac.readInlineTemplate(ll[0].nodeId, [":ConfirmedOn"]).then(function(co) {
                        item[name].confirmedOn = co.items[0].readItem.data;
                        return fac.readInlineTemplate(ll[0].nodeId, [":ConfirmedBy"]).then(function(co) {
                            item[name].confirmedBy = co.items[0].readItem.data;
                            return resultItem;
                        });
                    });
                }
                else
                    prom = Promise.resolve(item);
                return prom;
            }));
        }
    });
    return Promise.all(promises);
};

utils.createDefinitionRoot = function(fac) {
    return utils.createLabelledEntity(fac, fac.appId, metaLabel, definitionRootLabel, { createMetaLabelField: true });
};

utils.createDataRoot = function(fac) {
    return utils.createLabelledEntity(fac, fac.appId, metaLabel, dataRootLabel);
};

utils.createLabelListRoot = function(fac, defnRoot) {
    return utils.createLabelledEntity(fac, defnRoot, metaLabel, labelListRootLabel);
};

utils.createTypesRoot = function(fac, defnRoot) {
    return utils.createLabelledEntity(fac, defnRoot, metaLabel, typesRootLabel);
};

utils.createTemplateRoot = function(fac, defnRoot) {
    return utils.createLabelledEntity(fac, defnRoot, metaLabel, templateRootLabel);
};

utils.prime = function(fac) {
    return utils.createDefinitionRoot(fac).then(function(defnRootNode) {
        var defnRoot = defnRootNode.nodeId;
        var promises = [];
        promises.push(utils.createLabelListRoot(fac, defnRoot).then(function(llr) {
            var labelListRoot = llr.nodeId;
            return fac.createLabelList(labelListRoot, confirmationStatusName, ["Confirmed", "Rejected"]).then(function(cllr) {
                return Promise.all([
                    fac.createField(labelListRoot, "ConfirmedOn", { uniqueByParent: true }),
                    fac.createField(labelListRoot, "ConfirmedBy", { uniqueByParent: true })
                ]);
            });
        }));
        
        promises.push(utils.createTypesRoot(fac, defnRoot).then(function(tr) {
            return utils.createTemplateRoot(fac, defnRoot).then(function(tmplr) {
                var templatePromises = [];
                Object.keys(templates).forEach(function(key) {
                    templatePromises.push(utils.createFieldTypes(fac, templates[key], tr.nodeId).then(function(cftr) {
                        return fac.createTemplate(tmplr.nodeId, key, templates[key]);
                    }));
                });
                return Promise.all(templatePromises);
            });
        }));

        promises.push(utils.createDataRoot(fac));
        
        promises.push(fac.createField(defnRoot, userLabel, { searchable: true }));

        return Promise.all(promises);
    });
};

utils.getDigipassUserRoot = function(fac, digipassUser) {
    return fac.describe("[" + userLabel + "=" + digipassUser + "]").then(function(reply) {
        return reply.node.nodeId;
    }).catch(function(err) {
        return utils.createLabelledEntity(fac, "[MetaLabel=" + dataRootLabel + "]", userLabel, digipassUser).then(function(created) {
            return created.nodeId;
        });
    });
};

utils.write = function(fac, userRoot, incoming) {
    var flatten = function(inc) {
        var res = [];
        inc.forEach(function(item) {
            var name = Object.keys(item)[0];
            var resItem = {};
            if (Array.isArray(item[name]))
                resItem[name] = flatten(item[name]);
            else
                resItem[name] = item[name].data;
            res.push(resItem);
        });
        return res;
    };
    var doc = flatten(incoming);
    return fac.writeDocument(userRoot, doc).then(function(a) {
        var markConfirmed = function(inc, res) {
            var promises = [];
            inc.forEach(function(item, idx) {
                var name = Object.keys(item)[0];
                if (Array.isArray(item[name]))
                    promises.push(markConfirmed(item[name], res[idx].children));
                else {
                    if (item[name].hasOwnProperty('confirmedBy')) {
                        var nodeId = res[idx].nodeId;
                        promises.push(fac.labelNode(nodeId, 'frn:labellistmember::' + confirmationStatusName + '/Confirmed').then(function(r) {
                            return Promise.all([
                                Promise.resolve(r),
                                fac.createInformation(r.nodeId, ":ConfirmedBy", item[name].confirmedBy),
                                fac.createInformation(r.nodeId, ":ConfirmedOn", item[name].confirmedOn)
                            ]);
                        }));
                    }
                }
            });
            return Promise.all(promises);
        };
        return Promise.all([Promise.resolve(a), markConfirmed(incoming, a.nodes)]);
    });
};

utils.read = function(fac, digipassUser, template) {
    return utils.getDigipassUserRoot(fac, digipassUser).then(function(userRoot) {
        return fac.readTemplate(userRoot, ":" + template).then(function(r) {
            var coalesceResult = utils.coalesceTemplateReadName(fac, template, r);
            return utils.addConfirmationStatus(fac, coalesceResult);
        });
    });
};

utils.getTemplateFieldNames = function(templateName) {
    var flatten = function(inc) {
        var res = [];
        inc.forEach(function(item) {
            if (typeof item === 'string' || item instanceof String)
                res.push(item.replace(/^:/, ''));
            else
                res = res.concat(flatten(item[Object.keys(item)[0]]))
        });
        return res;
    };
    return flatten(templates[templateName]);
};
