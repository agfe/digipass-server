var express = require('express');
var bodyparser = require('body-parser');
var expressSession = require('express-session');
var cors = require('cors');
var oauthServer = require('oauth2-server');
var auth = require('http-auth');

var basic = auth.basic({
  realm: "Digital Password Demo",
  file: __dirname + "users.htpasswd"
});

var app = express();

var model = require('./model');
var Factern = require('./factern').Factern;
var utils = require('./utils');

app.use('/scripts', express.static(__dirname + '/../node_modules/bootstrap/dist/'));
app.use('/static', express.static(__dirname + '/static'))

app.oauth = new oauthServer({ model: model, allowEmptyState: true });

app.use(expressSession({ secret: process.env.SESSION_SECRET, resave: true, saveUninitialized: true }));

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));

app.set('view engine', 'pug');
app.set('views', __dirname + '/views');

app.use(cors());

var appId = process.env.FACTERN_APP_ID;
var appKey = process.env.FACTERN_APP_KEY;
var userId = process.env.FACTERN_USER_ID;

const Request = oauthServer.Request;
const Response = oauthServer.Response;

let authHandler = {
  handle: function(request, response) {
    return request.session.user;
  }
};

function authenticate(options) {
    return function(req, res, next) {
        let request = new Request(req);
        let response = new Response(res);
        app.oauth.authenticate(request, response, options).then(function(token) {
            res.locals.oauth = {token: token};
            req.user = token.user;
            next();
        }).catch(function (err) {
            res.status(err.code || 500).json(err)
        });
    }
}

function loginUser() {
    return function(req, res, next) {
        app.oauth.options.model.getUser(req.body.username, req.body.password).then(function(result) {
            req.session.user = result;
            next();
        });
    }
}

function authorize(options) {
    return function(req, res, next) {
        let request = new Request(req);
        let response = new Response(res);
        app.oauth.authorize(request, response, options).then(function(code) {
            res.redirect(code.redirectUri + '?code=' + code.authorizationCode);
            next();
        }).catch(function (err) {
            res.status(err.code || 500).json(err)
        });
    }
}

function token() {
    return function(req, res, next) {
        let request = new Request(req);
        let response = new Response(res);
        app.oauth.token(request, response).then(function(token) {
            res.json(token)
            next();
        }).catch(function (err) {
            res.status(err.code || 500).json(err)
        });
    }
}

function authItemsFromScope(scope) {
    var authItems = [];
    var scopes = scope.split(" ");
    scopes.forEach(function(scope, idx) {
        var split = scope.split(":");
        var fields = utils.getTemplateFieldNames(split[0]);
        authItems.push({
            id: idx,
            template: split[0],
            fields: fields
        });
    });
    return authItems;
}

app.get('/login', function (req, res) {
    res.render('login', {
        redirect: req.query.redirect,
        client_id: req.query.client_id,
        redirect_uri: req.query.redirect_uri,
        scope: req.query.scope
    });
});

app.post('/login', loginUser(), function (req, res) {
    if (!req.session.user) {
        res.render('login', {
            redirect: req.body.redirect,
            client_id: req.body.client_id,
            redirect_uri: req.body.redirect_uri,
            scope: req.body.scope
        });
        return;
    }

    return res.redirect('/oauth/authorise' + '?client_id=' + req.body.client_id + '&redirect_uri=' + encodeURIComponent(req.body.redirect_uri) + '&scope=' + encodeURIComponent(req.body.scope));
});

app.get('/oauth/authorise', function (req, res, next) {
    if (!req.session.user)
        return res.redirect('/login?redirect=' + req.path + '&client_id=' + req.query.client_id + '&redirect_uri=' + encodeURIComponent(req.query.redirect_uri) + '&scope=' + encodeURIComponent(req.query.scope));
    var authItems = authItemsFromScope(req.query.scope);
    app.oauth.options.model.getClientName(req.query.client_id).then(function(clientName) {
        res.render('authorise', {
            client_id: req.query.client_id,
            client_name: clientName,
            redirect_uri: req.query.redirect_uri,
            scope: req.query.scope,
            auth_items: authItems
        });
    });
});

app.post('/oauth/authorise', authorize({authenticateHandler: authHandler}));

app.all('/oauth/token', token());

app.get('/user', authenticate(), function(request, response) {
    console.log(request.user);
    response.send(request.user);
});

app.post('/write', authenticate(), function(request, response) {
    var fac = new Factern(appId, appKey, userId);
    var digipassUser = request.user.id;
    var templates = request.body.templates;
    utils.getDigipassUserRoot(fac, digipassUser).then(function(userRoot) {
        var promises = [];
        templates.forEach(function(template) {
            promises.push(utils.write(fac, userRoot, template));
        });
        Promise.all(promises).then(function(r) {
            console.log(JSON.stringify(r, null, 2));
            response.send(r);
        });
    });
});

app.post('/read', authenticate(), function(request, response) {
    var fac = new Factern(appId, appKey, userId);
    var digipassUser = request.user.id;
    var templateNames = request.body.templateNames;
    var promises = [];
    templateNames.forEach(function(templateName) {
        promises.push(utils.read(fac, digipassUser, templateName))
    });
    Promise.all(promises).then(function(r) {
        console.log(JSON.stringify(r, null, 2));
        response.send(r);
    });
});

app.get('/admin', auth.connect(basic), function(request, response) {
  response.render('admin', {});
});

app.post('/admin', auth.connect(basic), function (request, response) {
    var fac = new Factern(appId, appKey, userId);
    utils.deleteAll(fac, appId, false).then(function(r) {
        console.log(JSON.stringify(r, null, 2));
        utils.prime(fac).then(function(a) {
            console.log(JSON.stringify(a, null, 2));
            response.send(a);
        });
    });
});

app.listen(8081);
